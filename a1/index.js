/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInput(){
		let fullName = prompt("What is your Full Name? ");
		let age = prompt("How old are you? ");
		let location = prompt("Where do you live? ");

		console.log("Hello, " + fullName + ".");
		console.log("You are " + age + " years old.");
		console.log("You live in " + location + ".");
	};

	userInput();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topMusicBand(){
		console.log("My favorite Music Artist: ");
		console.log("1. Taylor Swift \n2. Billie Eilish \n3. NIKI \n4. Lana Del Rey \n5. Planetboom");

	};

	topMusicBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function topMovies(){
		console.log("My favorite Movies: ");
		console.log("1. The Menu \n Rotten Tomatoes Rating: 88% \n2. Avatar \n Rotten Tomatoes Rating: 77% \n3. You \n Rotten Tomatoes Rating: 91% \n4. The Last of Us \n Rotten Tomatoes Rating: 96% \n5. Elvis \n Rotten Tomatoes Rating: 77%");
	};

	topMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	// printUsers();
	// let printFriends() = function printUsers(){
	let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		// let friend1 = alert("Enter your first friend's name:");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		// console.log(friends); 
		console.log(friend3);
	};

	printFriends();


	// console.log(friend1);
	// console.log(friend2);